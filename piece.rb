require_relative "error"

class Piece
  
  BACK_ROW = { :red => 9, :black => 0 }
  
  attr_reader :color, :board
  attr_accessor :crowned, :value, :pos
  
  def initialize(color, pos, board, crowned = false)
    raise "Invalid color" if ![:red, :black].include?(color)
    @color = color
    @pos = pos
    @board = board
    @value = "\u265F".encode('utf-8')
    @crowned = crowned
  end
    
  def render
    print " #{self.value} ".colorize(:color => self.color, :background => :light_black)
  end

  def crowned?
    @crowned
  end

  def deltas
    red   = [ [ 1, -1], [ 1, 1] ]
    black = [ [-1, -1], [-1, 1] ]
    
    if self.crowned?
      red + black
    else
      (self.color == :red) ? red : black
    end
    
  end

  def is_enemy?(pos)
    return false if !self.board.on_board?(pos) || self.board[pos].nil?
    # return false if self.board[pos].nil?
    self.board[pos].color != self.color 
  end
  
  def all_moves
    self.slide_moves + self.jump_moves
  end

  def slide_moves
    slide_moves = []
    deltas.each do |delta|
      possible_slide = [self.pos[0] + delta[0], self.pos[1] + delta[1]]
      
      slide_moves << possible_slide if self.board.empty_square?(possible_slide) 
    end
    slide_moves
  end
  
  def jump_moves
    jump_moves = []
    deltas.each do |delta|
      adjacent_square = [self.pos[0] + delta[0], self.pos[1] + delta[1]]
      possible_jump   = [self.pos[0] + (delta[0]*2), self.pos[1] + (delta[1]*2)]
      
      jump_moves << possible_jump if self.board.empty_square?(possible_jump) && 
                                     self.is_enemy?(adjacent_square)
    end
    jump_moves
  end
  
  def dup_piece(dup_board)
    dup_piece = Piece.new(self.color, self.pos, dup_board, self.crowned)
  end
  
  def perform_moves(move_sequence)
    if valid_move_seq?(move_sequence)
      perform_moves!(move_sequence)
    else
      raise InvalidMoveError.new("Invalid move sequence!!!")
    end
  end
  
  def perform_moves!(move_sequence)
    moves_made = []
    
    move_sequence.each do |end_pos|
      if self.perform_slide?(end_pos) && (moves_made.empty?)
        perform_move!(end_pos)
        moves_made << "S"
      elsif self.perform_jump?(end_pos) && (!moves_made.include?("S"))
        self.board[self.enemy_jumped_pos(end_pos)] = nil
        perform_move!(end_pos)
        moves_made << "J"
      else
        raise InvalidMoveError.new("Cannot make that move: #{end_pos}")
      end
    end
  end
  
  def perform_move!(end_pos)
    self.board[self.pos] = nil
    self.board[end_pos] = self
    self.pos = end_pos
    
    if self.pos[0] == BACK_ROW[self.color]
      @crowned = true
    end
  end
  
  def valid_move_seq?(move_sequence)
    dup_board = self.board.dup
    begin
      dup_board[self.pos].perform_moves!(move_sequence)
    rescue 
      return false
    end
    return true
  end

  def perform_slide?(slide_pos)
    self.slide_moves.include?(slide_pos)
  end
  
  def enemy_jumped_pos(jump_pos)
    enemy_pos = [ (self.pos[0] + jump_pos[0])/2  , (self.pos[1] + jump_pos[1])/2 ]
  end
  
  def perform_jump?(jump_pos)
    self.jump_moves.include?(jump_pos)
  end
  
  

end