require_relative "board"
require_relative "error"

class Game

  
  COLS = { "A" => 0,
           "B" => 1,
           "C" => 2,
           "D" => 3,
           "E" => 4,
           "F" => 5,
           "G" => 6,
           "H" => 7,
           "I" => 8,
           "J" => 9 }
  
  attr_reader :board, :red_player, :black_player
  
  def initialize
    @board = Board.new
  end
  
  def play
    until game_over?(:red) || game_over?(:black)
      self.board.render
      make_move(:red)
      
      self.board.render
      make_move(:black)
    end
    
    puts "Red wins!" if game_over?(:black)
    puts "Black wins!" if game_over?(:red)
  end
  
  def make_move(color)
    puts "#{color.upcase} Player's turn"
    
    begin
      puts "Which piece would you like to move?"
      start_pos = convert_pos(gets.chomp.upcase)
      
      raise InvalidMoveError.new("No piece there") if self.board[start_pos].nil?
      raise InvalidMoveError.new("Not your piece") if (self.board[start_pos].color != color)
      
      puts "Where would you like to move this piece?"
      move_seq = convert_seq(gets.chomp)
      self.board[start_pos].perform_moves(move_seq)
    rescue InvalidMoveError => e
      puts "#{e.message}! Pick again"
      retry
    end
    
  end
  
  def convert_pos(str)
    str_array = str.split("")
    [str_array[1].to_i , COLS[str_array[0]]]
  end
  
  def convert_seq(str)
    sequences_arr = str.split(",")
    sequences_arr.map { |seq| convert_pos(seq.strip.upcase) }   
  end

    def game_over?(color)
      all_pieces_captured?(color) || no_legal_moves?(color)
    end

    def all_pieces_captured?(color)
      self.board.all_pieces.none? { |piece| piece.color == color }
    end

    def no_legal_moves?(color)
      color_pieces = self.board.all_pieces.select { |piece| piece.color == color }
      color_pieces.all? { |piece| piece.all_moves.empty? }
    end
  
end



