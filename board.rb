require_relative "piece"
require 'colorize'

class Board

  attr_accessor :grid

  def initialize(populate_board = true)
    @grid = Array.new(10) { Array.new(10, nil) }
    
    populate_red if populate_board
    populate_black if populate_board
  end
  
  def []=(pos, element)
    self.grid[pos.first][pos.last] = element
  end

  def [](pos)
    self.grid[pos.first][pos.last]
  end
  
  def populate_red
    initial_rows = [0, 1, 2]
    initial_rows.each do |row_idx|
      (0..9).each do |col_idx|
        self[[row_idx, col_idx]] = Piece.new(:red, [row_idx, col_idx], self) if                                    correct_sq_color?([row_idx, col_idx])
      end
    end
  end

  def populate_black
    initial_rows = [7, 8, 9]
    initial_rows.each do |row_idx|
      (0..9).each do |col_idx|
        self[[row_idx, col_idx]] = Piece.new(:black, [row_idx, col_idx], self) if                                    correct_sq_color?([row_idx, col_idx])
      end
    end
  end
  
  def correct_sq_color?(pos)
    (pos.first.even? && pos.last.even?) || (pos.first.odd? && pos.last.odd?)
  end
  
  def render
    print "   "
    ("A".."J").each { |col_ref| print " #{col_ref} " }
    print "\n"

    self.grid.each_with_index do |row, row_idx|
      print " #{row_idx} "
      row.each_with_index do |el, col_idx|
        if el.nil?
          if correct_sq_color?([row_idx, col_idx])
            print "   ".colorize( :background => :light_black )
          else
            print "   ".colorize( :background => :white )
          end
        else
          el.render
        end
      end
      print "\n"
    end

  end
  
  def empty_square?(pos)
     on_board?(pos) && self[pos].nil?
  end
  
  def on_board?(pos)
    pos.all? { |coord| (0..9).include?(coord) }
  end
  
  def dup
    dup_board = Board.new(false)
    all_pieces.each do |piece|
      dup_board[piece.pos] = piece.dup_piece(dup_board)
    end
    dup_board
  end
  
  def all_pieces
    self.grid.flatten.compact
  end
  
end
